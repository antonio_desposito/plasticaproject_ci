<?php

namespace App\Controllers;

use App\Controllers\BaseController;
include(APPPATH . 'Libraries/GroceryCrudEnterprise/autoload.php');

use GroceryCrud\Core\GroceryCrud;

class TipiMateriale extends BaseController
{
	public function index()
	{
		$crud = $this->_getGroceryCrudEnterprise();

		$crud->setCsrfTokenName(csrf_token());
		$crud->setCsrfTokenValue(csrf_hash());
		$crud->unsetBootstrap();

		$crud->setTable('TipoMateriale');
		$crud->setSubject('Tipo Materiale', 'Tipi Materiale');
		$systemColums = ['created_at','updated_at','deleted_at'];
		$crud->unsetColumns($systemColums);
		$crud->unsetFields($systemColums);
		//$crud->columns(['nome', 'codice', 'colorato']);
		$output = $crud->render();

		return $this->_example_output($output);
	}

	private function _example_output($output = null)
	{
		if (isset($output->isJSONResponse) && $output->isJSONResponse) {
			header('Content-Type: application/json; charset=utf-8');
			echo $output->output;
			exit;
		}

		return view('dashboard/settings', (array)$output);
	}

	private function _getDbData()
	{
		$db = (new \Config\Database())->default;
		return [
			'adapter' => [
				'driver' => 'Pdo_Mysql',
				'host'     => $db['hostname'],
				'database' => $db['database'],
				'username' => $db['username'],
				'password' => $db['password'],
				'charset' => 'utf8'
			]
		];
	}

	private function _getGroceryCrudEnterprise($bootstrap = true, $jquery = true)
	{
		$db = $this->_getDbData();
		$config = (new \Config\GroceryCrudEnterprise())->getDefaultConfig();

		$groceryCrud = new GroceryCrud($config, $db);
		return $groceryCrud;
	}
}
