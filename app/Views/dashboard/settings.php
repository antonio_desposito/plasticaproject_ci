<?= $this->extend('templates/material') ?>

<?= $this->section('custom_head'); ?>
<?php foreach ($css_files as $file) : ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?= $this->endSection(); ?>

<?= $this->section('navbar_wrapper') ?>
<a class="navbar-brand" href="javascript:;">Configurazione</a>
<?= $this->endSection() ?>

<?= $this->section('content'); ?>
<?php echo $output; ?>
<?= $this->endSection(); ?>

<?= $this->section('custom_script'); ?>
ciao mondo !!!
<?php foreach ($js_files as $file) : ?>
   
    <script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<?= $this->endSection(); ?>