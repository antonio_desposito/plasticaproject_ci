<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateListinoPrezzi extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id' => [
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => true,
				'auto_increment' => true,
			],
			'DataInizio' => [
				'type' => 'DATETIME',
				'null' => true,
			],
			'DataFine' => [
				'type' => 'DATETIME',
				'null' => true,
			],
			'FornitoreId' => [
				'type' => 'INT',
				'constraint' => 11,
				'null' => true,
			],
			'Colorato'      => [
				'type'           => 'ENUM',
				'constraint'     => ['No', 'Si'],
				'default'        => 'No',
			],
			'Prezzo' => [
				'type' => 'FLOAT',
				'null' => true,
			],
			'created_at' => [
				'type' => 'DATETIME',
				'null' => true,
			],
			'updated_at' => [
				'type' => 'DATETIME',
				'null' => true,
			],
			'deleted_at' => [
				'type' => 'DATETIME',
				'null' => true,
			],
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable('ListinoPrezzi');
	}

	public function down()
	{
		$this->forge->dropTable('ListinoPrezzi');
	}
}
