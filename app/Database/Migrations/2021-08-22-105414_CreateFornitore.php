<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateFornitore extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id' => [
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => true,
				'auto_increment' => true,
			],
			'RagioneSociale' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
			],
			'Codice' => [
				'type' => 'VARCHAR',
				'constraint' => 4,
			],
			'Descrizione' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
			],
			'Indirizzo' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
			],
			'Citta' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
			],
			'Provincia' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
			],
			'CAP' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
			],
			'TipoFornitore' => [
				'type'           => 'ENUM',
				'constraint'     => ['MP', 'PF', 'MP-PF'],
				'default'        => 'MP',
			],
			'Attivo' => [
				'type'           => 'ENUM',
				'constraint'     => ['No', 'Si'],
				'default'        => 'Si',
			],
			'created_at' => [
				'type' => 'DATETIME',
				'null' => true,
			],
			'updated_at' => [
				'type' => 'DATETIME',
				'null' => true,
			],
			'deleted_at' => [
				'type' => 'DATETIME',
				'null' => true,
			],
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable('Fornitori');
	}

	public function down()
	{
		$this->forge->dropTable('Fornitori');
	}
}
