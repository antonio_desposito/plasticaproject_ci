<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateTipoMateriale extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id' => [
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => true,
				'auto_increment' => true,
			],
			'Nome' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
			],
			'Codice' => [
				'type' => 'VARCHAR',
				'constraint' => 4,
			],
			'PesoSpecifico' => [
				'type' => 'INT',
				'constraint' => 11,
				'null' => true,
			],
			'created_at' => [
				'type' => 'DATETIME',
				'null' => true,
			],
			'updated_at' => [
				'type' => 'DATETIME',
				'null' => true,
			],
			'deleted_at' => [
				'type' => 'DATETIME',
				'null' => true,
			],
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable('TipoMateriale');
	}

	public function down()
	{
		$this->forge->dropTable('TipoMateriale');
	}
}
